﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ChatApp.Admin.Startup))]
namespace ChatApp.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
