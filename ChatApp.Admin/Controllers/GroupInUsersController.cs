﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ChatApp.Data;
using ChatApp.Data.Repositories;

namespace ChatApp.Admin.Controllers
{
    public class GroupInUsersController : Controller
    {
        private IGroupInUserRepository GroupInUserRepository { get; set; }
        private IGroupRepository GroupRepository { get; set; }
        private IUserDetailsRepository UserDetailsRepository { get; set; }

        public GroupInUsersController(IGroupInUserRepository groupInUserRepository, IGroupRepository groupRepository,
            IUserDetailsRepository userDetailsRepository)
        {
            GroupInUserRepository = groupInUserRepository;
            GroupRepository = groupRepository;
            UserDetailsRepository = userDetailsRepository;
        }

        // GET: GroupInUsers
        public ActionResult Index()
        {
            return View(GroupInUserRepository.GetAllProc());
        }

        // GET: GroupInUsers/Details/5
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var groupInUser = GroupInUserRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (groupInUser == null)
            {
                return HttpNotFound();
            }
            return View(groupInUser);
        }

        // GET: GroupInUsers/Create
        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(GroupRepository.GetAllProc(), "Id", "Name");
            ViewBag.UserDetailsId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName");
            return View();
        }

        // POST: GroupInUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GroupId,UserDetailsId")] GroupInUser groupInUser)
        {
            if (ModelState.IsValid)
            {
                GroupInUserRepository.Add(groupInUser);
                GroupInUserRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(GroupInUserRepository.GetAllProc(), "Id", "Name", groupInUser.GroupId);
            ViewBag.UserDetailsId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", groupInUser.UserDetailsId);
            return View(groupInUser);
        }

        // GET: GroupInUsers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var groupInUser = GroupInUserRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (groupInUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(GroupRepository.GetAllProc(), "Id", "Name", groupInUser.GroupId);
            ViewBag.UserDetailsId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", groupInUser.UserDetailsId);
            return View(groupInUser);
        }

        // POST: GroupInUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GroupId,UserDetailsId")] GroupInUser groupInUser)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.GroupId = new SelectList(GroupRepository.GetAllProc(), "Id", "Name", groupInUser.GroupId);
                ViewBag.UserDetailsId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", groupInUser.UserDetailsId);
                return View(groupInUser);
            }
            var dbGroupInUser = GroupInUserRepository.GetBy(x => x.Id == groupInUser.Id).FirstOrDefault();
            UpdateModel(dbGroupInUser);
            GroupInUserRepository.Save();

            return RedirectToAction("Index");
            
        }

        // GET: GroupInUsers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var groupInUser = GroupInUserRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (groupInUser == null)
            {
                return HttpNotFound();
            }
            return View(groupInUser);
        }

        // POST: GroupInUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var groupInUser = GroupInUserRepository.GetBy(x => x.Id == id).FirstOrDefault();
            GroupInUserRepository.Remove(groupInUser);
            GroupInUserRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                GroupRepository.Dispose();
                UserDetailsRepository.Dispose();
                GroupInUserRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
