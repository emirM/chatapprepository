﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ChatApp.Data;
using ChatApp.Data.Repositories;

namespace ChatApp.Admin.Controllers
{
    public class FriendsController : Controller
    {
        private IFriendsRepository FriendsRepository { get; set; }
        private IUserDetailsRepository UserDetailsRepository { get; set; }

        public FriendsController(IFriendsRepository friendsRepository, IUserDetailsRepository userDetailsRepository)
        {
            FriendsRepository = friendsRepository;
            UserDetailsRepository = userDetailsRepository;
        }

        // GET: Friends
        public ActionResult Index()
        {
            return View(FriendsRepository.GetAllProc());
        }

        // GET: Friends/Details/5
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var friend = FriendsRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (friend == null)
            {
                return HttpNotFound();
            }
            return View(friend);
        }

        // GET: Friends/Create
        public ActionResult Create()
        {
            ViewBag.UserDetailsId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName");
            return View();
        }

        // POST: Friends/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserDetailsId,UserFriendsId")] Friend friend)
        {
            if (ModelState.IsValid)
            {
                FriendsRepository.Add(friend);
                FriendsRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.UserDetailsId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", friend.UserDetailsId);
            return View(friend);
        }

        // GET: Friends/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var friend = FriendsRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (friend == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserDetailsId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", friend.UserDetailsId);
            return View(friend);
        }

        // POST: Friends/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserDetailsId,UserFriendsId")] Friend friend)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.UserDetailsId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", friend.UserDetailsId);
                return View(friend);
            }
            var dbFriend = FriendsRepository.GetBy(x => x.Id == friend.Id).FirstOrDefault();
            UpdateModel(dbFriend);
            FriendsRepository.Save();

            return RedirectToAction("Index");
        }

        // GET: Friends/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var friend = FriendsRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (friend == null)
            {
                return HttpNotFound();
            }
            return View(friend);
        }

        // POST: Friends/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var friend = FriendsRepository.GetBy(x => x.Id == id).FirstOrDefault();
            FriendsRepository.Remove(friend);
            FriendsRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserDetailsRepository.Dispose();
                FriendsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
