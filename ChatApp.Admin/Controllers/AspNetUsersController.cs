﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ChatApp.Data;
using ChatApp.Data.Repositories;

namespace ChatApp.Admin.Controllers
{
    public class AspNetUsersController : Controller
    {
        private IAspNetUsersRepository AspNetUsersRepository { get; set; }
        private IAspNetUserLoginsRepository AspNetUserLoginsRepository { get; set; }
        private IAspNetUserClaimsRepository AspNetUserClaimsRepository { get; set; }

        public AspNetUsersController(IAspNetUsersRepository aspNetUsersRepository, IAspNetUserLoginsRepository aspNetUserLoginsRepository,
            IAspNetUserClaimsRepository aspNetUserClaimsRepository)
        {
            AspNetUsersRepository = aspNetUsersRepository;
            AspNetUserLoginsRepository = aspNetUserLoginsRepository;
            AspNetUserClaimsRepository = aspNetUserClaimsRepository;
        }

        // GET: AspNetUsers
        public ActionResult Index()
        {
            return View(AspNetUsersRepository.GetAllProc());
        }

        // GET: AspNetUsers/Details/5
        public ActionResult Details(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var aspNetUser = AspNetUsersRepository.GetByIdString(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AspNetUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] AspNetUser aspNetUser)
        {
            if (!ModelState.IsValid) return View(aspNetUser);
            AspNetUsersRepository.Add(aspNetUser);
            AspNetUsersRepository.Save();

            return RedirectToAction("Index");
        }

        // GET: AspNetUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var aspNetUser = AspNetUsersRepository.GetByIdString(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] AspNetUser aspNetUser)
        {
            if (!ModelState.IsValid) return View(aspNetUser);

            var dbAspNetUser = AspNetUsersRepository.GetByIdString(aspNetUser.Id);
            UpdateModel(dbAspNetUser);
            AspNetUsersRepository.Save();

            return RedirectToAction("Index");
        }

        // GET: AspNetUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var aspNetUser = AspNetUsersRepository.GetByIdString(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            //AspNetUserLogins
            var aspNetUserLogin =AspNetUserLoginsRepository.GetBy(x => x.UserId == id).FirstOrDefault();
            if (aspNetUserLogin != null)
            {
                AspNetUserLoginsRepository.Remove(aspNetUserLogin);
                AspNetUserLoginsRepository.Save();
            }
            
            /*************************/
            //AspNetUserClaims
            var aspNetUserClaim = AspNetUserClaimsRepository.GetBy(x => x.UserId == id).FirstOrDefault();
            if (aspNetUserClaim != null)
            {
                AspNetUserClaimsRepository.Remove(aspNetUserClaim);
                AspNetUserClaimsRepository.Save();
            }
            
            /****************************/

            var aspNetUser = AspNetUsersRepository.GetByIdString(id);
            AspNetUsersRepository.SuperDelete(aspNetUser);
            AspNetUsersRepository.Remove(aspNetUser);
            AspNetUsersRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                AspNetUsersRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
