﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using ChatApp.Data;
using ChatApp.Data.Repositories;

namespace ChatApp.Admin.Controllers
{
    public class UserDetailsController : Controller
    {
        private IUserDetailsRepository UserDetailsRepository { get; set; }
        private IAspNetUsersRepository AspNetUsersRepository { get; set; }

        public UserDetailsController(IUserDetailsRepository userDetailsRepository,
            IAspNetUsersRepository aspNetUsersRepository)
        {
            UserDetailsRepository = userDetailsRepository;
            AspNetUsersRepository = aspNetUsersRepository;
        }

        // GET: UserDetails
        public ActionResult Index()
        {
            return View(UserDetailsRepository.GetAll().ToList());
        }

        // GET: UserDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userDetail = UserDetailsRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (userDetail == null)
            {
                return HttpNotFound();
            }
            return View(userDetail);
        }

        // GET: UserDetails/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(AspNetUsersRepository.GetAllProc(), "Id", "Email");
            return View();
        }

        // POST: UserDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Birthday,Sex,Avatar,UserId")] UserDetail userDetail)
        {
            if (ModelState.IsValid)
            {
                UserDetailsRepository.Add(userDetail);
                UserDetailsRepository.Save();

                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(AspNetUsersRepository.GetAllProc(), "Id", "Email", userDetail.UserId);
            return View(userDetail);
        }

        // GET: UserDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userDetail = UserDetailsRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (userDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(AspNetUsersRepository.GetAllProc(), "Id", "Email", userDetail.UserId);
            return View(userDetail);
        }

        // POST: UserDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Birthday,Sex,Avatar,UserId")] UserDetail userDetail)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.UserId = new SelectList(AspNetUsersRepository.GetAllProc(), "Id", "Email", userDetail.UserId);
                return View(userDetail);
            }

            var dbUserDetail = UserDetailsRepository.GetBy(x => x.Id == userDetail.Id);
            UpdateModel(dbUserDetail);
            UserDetailsRepository.Save();
            
            return RedirectToAction("Index");
            
        }

        // GET: UserDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userDetail = UserDetailsRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (userDetail == null)
            {
                return HttpNotFound();
            }
            return View(userDetail);
        }

        // POST: UserDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var userDetail = UserDetailsRepository.GetBy(x => x.Id == id).FirstOrDefault();
            UserDetailsRepository.Remove(userDetail);
            UserDetailsRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                AspNetUsersRepository.Dispose();
                UserDetailsRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
