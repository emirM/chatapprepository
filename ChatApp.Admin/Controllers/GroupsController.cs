﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ChatApp.Data;
using ChatApp.Data.Repositories;

namespace ChatApp.Admin.Controllers
{
    public class GroupsController : Controller
    {
        private IGroupRepository GroupRepository { get; set; }

        public GroupsController(IGroupRepository groupRepository)
        {
            GroupRepository = groupRepository;
        }

        // GET: Groups
        public ActionResult Index()
        {
            return View(GroupRepository.GetAllProc());
        }

        // GET: Groups/Details/5
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var group = GroupRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // GET: Groups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,DateCreate,AuthorUserDetailsId")] Group group)
        {
            if (!ModelState.IsValid) return View(@group);

            GroupRepository.Add(@group);
            GroupRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: Groups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var group = GroupRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,DateCreate,AuthorUserDetailsId")] Group group)
        {
            if (!ModelState.IsValid) return View(@group);

            var dbGroup = GroupRepository.GetBy(x => x.Id == group.Id);
            UpdateModel(dbGroup);
            GroupRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: Groups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var group = GroupRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var group = GroupRepository.GetBy(x => x.Id == id).FirstOrDefault();
            GroupRepository.Remove(group);
            GroupRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                GroupRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
