﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ChatApp.Data;
using ChatApp.Data.Repositories;

namespace ChatApp.Admin.Controllers
{
    public class MessegersController : Controller
    {
        private IMessegersRepository MessegersRepository { get; set; }
        private IUserDetailsRepository UserDetailsRepository { get; set; }

        public MessegersController(IMessegersRepository messegersRepository,
            IUserDetailsRepository userDetailsRepository)
        {
            MessegersRepository = messegersRepository;
            UserDetailsRepository = userDetailsRepository;
        }

        // GET: Messegers
        public ActionResult Index()
        {
            return View(MessegersRepository.GetAll());
        }

        // GET: Messegers/Details/5
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var messeger = MessegersRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (messeger == null)
            {
                return HttpNotFound();
            }
            return View(messeger);
        }

        // GET: Messegers/Create
        public ActionResult Create()
        {
            ViewBag.FromUserId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName");
            return View();
        }

        // POST: Messegers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DateCreate,Text,FromUserId,ToUserId,ToGroupId")] Messeger messeger)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.FromUserId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", messeger.FromUserId);
                return View(messeger);
            }
            MessegersRepository.Add(messeger);
            MessegersRepository.Save();
            return RedirectToAction("Index");
            
        }

        // GET: Messegers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var messeger = MessegersRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (messeger == null)
            {
                return HttpNotFound();
            }
            ViewBag.FromUserId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", messeger.FromUserId);
            return View(messeger);
        }

        // POST: Messegers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DateCreate,Text,FromUserId,ToUserId,ToGroupId")] Messeger messeger)
        {
            if (ModelState.IsValid)
            {
                ViewBag.FromUserId = new SelectList(UserDetailsRepository.GetAllProc(), "Id", "FirstName", messeger.FromUserId);
                return View(messeger);
            }
            var dbMesseger = MessegersRepository.GetBy(x => x.Id == messeger.Id).FirstOrDefault();
            UpdateModel(dbMesseger);
            MessegersRepository.Save();

            return RedirectToAction("Index");
        }

        // GET: Messegers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var messeger = MessegersRepository.GetBy(x=>x.Id == id).FirstOrDefault();
            if (messeger == null)
            {
                return HttpNotFound();
            }
            return View(messeger);
        }

        // POST: Messegers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var messeger = MessegersRepository.GetBy(x => x.Id == id).FirstOrDefault();
            MessegersRepository.Remove(messeger);
            MessegersRepository.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserDetailsRepository.Dispose();
                MessegersRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
