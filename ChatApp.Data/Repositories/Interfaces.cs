﻿using System.Collections.Generic;
using ChatApp.Data.Repositories.GenericRepository;

namespace ChatApp.Data.Repositories
{
    #region IAspNetUsersRepository

    public interface IAspNetUsersRepository : IGenericRepository<AspNetUser>
    {
        AspNetUser GetByIdString(string id);
        List<AspNetUser> GetAllProc();
        void SuperDelete(AspNetUser aspNetUser);
    }
    #endregion

    #region IUserDetailsRepoditory
    public interface IUserDetailsRepository : IGenericRepository<UserDetail>
    {
        List<UserDetail> GetAllProc();
    }
    #endregion

    #region IFriendsRepository

    public interface IFriendsRepository : IGenericRepository<Friend>
    {
        List<Friend> GetAllProc();
    }
    #endregion

    #region IMessegersRepository

    public interface IMessegersRepository : IGenericRepository<Messeger>
    {
        List<Messeger> GetAllProc();
    }
    #endregion

    #region IGroupInUserRepository

    public interface IGroupInUserRepository : IGenericRepository<GroupInUser>
    {
        List<GroupInUser> GetAllProc();
    }
    #endregion

    #region IGroupRepository

    public interface IGroupRepository : IGenericRepository<Group>
    {
        List<Group> GetAllProc();
    }
    #endregion

#region IAspNetUserClaimsRepository
    public interface IAspNetUserClaimsRepository : IGenericRepository<AspNetUserClaim> { }
#endregion

#region IAspNetUserLoginsRepository
    public interface IAspNetUserLoginsRepository : IGenericRepository<AspNetUserLogin> { }
#endregion
}