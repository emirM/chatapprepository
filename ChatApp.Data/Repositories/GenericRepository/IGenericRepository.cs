﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace ChatApp.Data.Repositories.GenericRepository
{
    public interface IGenericRepository<T> where T:class
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetBy(Expression<Func<T, bool>> predicate);
        T Add(T entity);
        void Remove(T entity);
        void Edit(T entity);
        void Save();
        void Dispose();
    }
}