﻿using System.Configuration;
using System.Data.Entity;

namespace ChatApp.Data.Repositories.GenericRepository
{
    public class Context<T> : IContext<T> where T:class
    {
        public DbContext DbContext { get; private set; }
        public IDbSet<T> DbSet { get; private set; }

        public Context()
        {
            DbContext = new DbContext(ConfigurationManager.ConnectionStrings["ChatAppEntities"].ConnectionString);
            DbSet = DbContext.Set<T>();
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}