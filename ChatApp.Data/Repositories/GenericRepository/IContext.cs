﻿using System;
using System.Data.Entity;

namespace ChatApp.Data.Repositories.GenericRepository
{
    public interface IContext<T> : IDisposable where T:class
    {
        DbContext DbContext { get; }
        IDbSet<T> DbSet { get; } 
    }
}