﻿using System.Collections.Generic;
using System.Linq;
using ChatApp.Data.Repositories.GenericRepository;

namespace ChatApp.Data.Repositories
{
    #region AspNetUsersRepository

    public class AspNetUsersRepository : GenericRepository<AspNetUser>, IAspNetUsersRepository
    {
        readonly ChatAppEntities _entities = new ChatAppEntities();

        public AspNetUser GetByIdString(string id)
        {
            return _entities.AspNetUsers.FirstOrDefault(x => x.Id == id);
        }

        public List<AspNetUser> GetAllProc()
        {
            return _entities.Database.SqlQuery<AspNetUser>("GetAllAspNetUsers").ToList();
        }

        public void SuperDelete(AspNetUser aspNetUser)
        {
            _entities.AspNetUsers.Remove(aspNetUser);
        }
    }
    #endregion

    #region UserDetailsRepoditory

    public class UserDetailsRepoditory : GenericRepository<UserDetail>, IUserDetailsRepository
    {
        readonly ChatAppEntities _entities = new ChatAppEntities();
        public List<UserDetail> GetAllProc()
        {
            return _entities.Database.SqlQuery<UserDetail>("GetAllUserDetails").ToList();
        } 
    }
    #endregion

    #region FriendsRepository

    public class FriendsRepository : GenericRepository<Friend>, IFriendsRepository
    {
        readonly ChatAppEntities _entities = new ChatAppEntities();
        public List<Friend> GetAllProc()
        {
            return _entities.Database.SqlQuery<Friend>("GetAllFriends").ToList();
        } 
    }
    #endregion

    #region MessegersRepository

    public class MessegersRepository : GenericRepository<Messeger>, IMessegersRepository
    {
        readonly ChatAppEntities _entities = new ChatAppEntities();
        public List<Messeger> GetAllProc()
        {
            return _entities.Database.SqlQuery<Messeger>("GetAllMessegers").ToList();
        } 
    }
    #endregion

    #region GroupInUserRepository

    public class GroupInUserRepository : GenericRepository<GroupInUser>, IGroupInUserRepository
    {
        readonly ChatAppEntities _entities = new ChatAppEntities();
        public List<GroupInUser> GetAllProc()
        {
            return _entities.Database.SqlQuery<GroupInUser>("GetAllGroupInUser").ToList();
        } 
    }
    #endregion

    #region GroupRepository

    public class GroupRepository : GenericRepository<Group>, IGroupRepository
    {
        readonly ChatAppEntities _entities = new ChatAppEntities();
        public List<Group> GetAllProc()
        {
            return _entities.Database.SqlQuery<Group>("GetAllGroup").ToList();
        } 
    }
    #endregion

    #region AspNetUserClaimsRepository

    public class AspNetUserClaimsRepository : GenericRepository<AspNetUserClaim>, IAspNetUserClaimsRepository
    {
        
    }
    #endregion

#region AspNetUserLoginsRepository
    public class AspNetUserLoginsRepository : GenericRepository<AspNetUserLogin>, IAspNetUserLoginsRepository { }
#endregion
}