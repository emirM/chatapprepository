﻿using System;
using System.Configuration;
using System.Web.Configuration;

namespace ChatApp.Data.Helpers
{
    internal class SettingsHelper
    {
        private static String emailSender = "";
        private static String emailPassword = "";
        private static Int32 emailSMTPPort = -1;
        private static String emailSMTP = "";


        public static String EmailSender
        {
            get
            {
                if (emailSender == "")
                {
                    var subject = WebConfigurationManager.AppSettings["EmailSender"];

                    if (String.IsNullOrEmpty(subject))
                        throw new ConfigurationErrorsException(
                            "Web Configuration file must contain <appSettings><add key=\"EmailSender\".../></appSettings>");
                    emailSender = subject;
                }

                return emailSender;
            }
        }

        public static String EmailPassword
        {
            get
            {
                if (emailPassword == "")
                {
                    var subject = WebConfigurationManager.AppSettings["EmailPassword"];

                    if (String.IsNullOrEmpty(subject))
                        throw new ConfigurationErrorsException(
                            "Web Configuration file must contain <appSettings><add key=\"EmailPassword\".../></appSettings>");
                    emailPassword = subject;
                }

                return emailPassword;
            }
        }

        public static String EmailSMTP
        {
            get
            {
                if (emailSMTP == "")
                {
                    var subject = WebConfigurationManager.AppSettings["EmailSMTP"];

                    if (String.IsNullOrEmpty(subject))
                        throw new ConfigurationErrorsException(
                            "Web Configuration file must contain <appSettings><add key=\"EmailSMTP\".../></appSettings>");
                    emailSMTP = subject;
                }

                return emailSMTP;
            }
        }

        public static Int32 EmailSMTPPort
        {
            get
            {
                if (emailSMTPPort == -1)
                {
                    var appSettingPortValue = WebConfigurationManager.AppSettings["EmailSMTPPort"];

                    if (String.IsNullOrEmpty(appSettingPortValue))
                        throw new ConfigurationErrorsException(
                            "Web Configuration file must contain <appSettings><add key=\"EmailSMTPPort\".../></appSettings>");

                    Int32 portValue;
                    if (!Int32.TryParse(appSettingPortValue, out portValue))
                        throw new ConfigurationErrorsException("key - \"EmailSMTPPort\" must contain integer value");

                    emailSMTPPort = portValue;
                }

                return emailSMTPPort;
            }
        }


    }

}
