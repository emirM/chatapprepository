﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace ChatApp.Data.Helpers
{
    public class EmailHelper
    {
        public static void SendMail(string emailAddress,string emailTitle,string emailText)
        {
            try
            {
                var loginInfo = new NetworkCredential(SettingsHelper.EmailSender, SettingsHelper.EmailPassword);
                var msg = new MailMessage();
                var smtpClient = new SmtpClient(SettingsHelper.EmailSMTP, SettingsHelper.EmailSMTPPort);

                msg.From = new MailAddress(SettingsHelper.EmailSender);
                msg.To.Add(new MailAddress(emailAddress));
                msg.Subject = emailTitle;
                msg.Body = emailText;
                msg.IsBodyHtml = true;

                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = loginInfo;
                smtpClient.Send(msg);
            }
            catch (Exception)
            {
                
            }
        }

        public static void SendMail(List<string> emailAddresses, string emailTitle, string emailText)
        {
            try
            {

                var loginInfo = new NetworkCredential(SettingsHelper.EmailSender, SettingsHelper.EmailPassword);
                var msg = new MailMessage();
                var smtpClient = new SmtpClient(SettingsHelper.EmailSMTP, SettingsHelper.EmailSMTPPort);

                msg.From = new MailAddress(SettingsHelper.EmailSender);
                foreach (var emailAddress in emailAddresses)
                    msg.To.Add(new MailAddress(emailAddress));

                msg.Subject = emailTitle;
                msg.Body = emailText;
                msg.IsBodyHtml = true;

                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = loginInfo;
                smtpClient.Send(msg);
            }
            catch (Exception)
            {

            }
        }
    }
}
