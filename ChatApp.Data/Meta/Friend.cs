﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatApp.Data
{
    [MetadataType(typeof(FriendMetaData))]
    public partial class Friend
    {
         
    }

    public class FriendMetaData
    {
        [Display(Name = "Пользователь")]
        [Required]
        public Nullable<int> UserDetailsId { get; set; }

        [Display(Name = "Друг")]
        [Required]
        public Nullable<int> UserFriendsId { get; set; }
    }
}