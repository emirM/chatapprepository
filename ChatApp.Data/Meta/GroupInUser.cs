﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatApp.Data
{
    [MetadataType(typeof(GroupInUserMetaData))]
    public partial class GroupInUser
    {
         
    }

    public class GroupInUserMetaData
    {
        [Display(Name = "Группа")]
        [Required]
        public Nullable<int> GroupId { get; set; }

        [Display(Name = "Пользователь")]
        [Required]
        public Nullable<int> UserDetailsId { get; set; }
    }
}