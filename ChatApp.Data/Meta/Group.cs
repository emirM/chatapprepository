﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatApp.Data
{
    [MetadataType(typeof(GroupMetaData))]
    public partial class Group
    {
         
    }

    public class GroupMetaData
    {
        [Display(Name = "Наименование")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Дата создание")]
        public Nullable<System.DateTime> DateCreate { get; set; }

        [Display(Name = "Автор")]
        [Required]
        public Nullable<int> AuthorUserDetailsId { get; set; }
    }
}