﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatApp.Data
{
    [MetadataType(typeof(MessegerMetaData))]
    public partial class Messeger
    {
         
    }

    public class MessegerMetaData
    {
        [Display(Name = "Дата создание")]
        [Required]
        public Nullable<System.DateTime> DateCreate { get; set; }

        [Display(Name = "Текст сообщения")]
        [Required]
        public string Text { get; set; }

        [Display(Name = "Отправитель")]
        [Required]
        public Nullable<int> FromUserId { get; set; }

        [Display(Name = "Получатель")]
        [Required]
        public Nullable<int> ToUserId { get; set; }

        [Display(Name = "Получатель группа")]
        [Required]
        public Nullable<int> ToGroupId { get; set; }
    }
}