﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatApp.Data
{
    [MetadataType(typeof(AspNetUserMetaData))]
    public partial class AspNetUser
    {

    }

    public class AspNetUserMetaData
    {
        public string Id { get; set; }

        [Display(Name = "Почта")]
        [Required]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }

        [Display(Name = "Телефон")]
        [Required]
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }

        [Display(Name = "Логин")]
        [Required]
        public string UserName { get; set; }
    }
}