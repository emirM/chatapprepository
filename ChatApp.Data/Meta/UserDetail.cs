﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatApp.Data
{
    [MetadataType(typeof(UserDetailMetaData))]
    public partial class UserDetail
    {
         
    }

    public class UserDetailMetaData
    {
        [Display(Name = "Имя")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        public string LastName { get; set; }

        [Display(Name = "Дата рождения")]
        //[Required]
        public Nullable<DateTime> Birthday { get; set; }

        [Display(Name = "Пол")]
        //[Required]
        public Nullable<bool> Sex { get; set; }

        [Display(Name = "Аватарка")]
        //[Required]
        public string Avatar { get; set; }

        [Display(Name = "Пользователь")]
        [Required]
        public string UserId { get; set; }
    }
}